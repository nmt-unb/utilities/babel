from django.core.management.base import BaseCommand
from django.core.management import call_command
from babel.models import Settings

class Command(BaseCommand):
    help = 'Fill database with data'

    def add_arguments(self, parser):
        parser.add_argument(
            '--reset',
            nargs='?',
            type=bool,
            const=True,
            default=False,
            help='Prompt reset before migrating'
        )

    def handle(self, *args, **options):
        if options['reset']: call_command('reset_db')
        call_command('migrate')

        seeds = {}

        if not self.has_seed_hash('base'):
            self.cookbook(seeds)
            self.add_seed_hash('base')
        else:
            print('\n# Data already seeded!')

    def has_seed_hash(self, context):
        try:
            settings = Settings.objects.get(id=1)
            return settings.has_seed(context)
        except:
            Settings.objects.create()
            return False

    def add_seed_hash(self, context):
        settings = Settings.objects.get(id=1)
        settings.add_seed(context)
        settings.save()

    def cookbook(self, seeds):
        print('\n# Seeding Data')

        from babel.seeds import UserSeeder
        UserSeeder().seed(seeds)
