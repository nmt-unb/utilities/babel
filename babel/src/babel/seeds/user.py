from django.contrib.auth import get_user_model
from utils.seeds import Seeder

class UserSeeder(Seeder):
    name = 'user'

    def elements(self):
        return {
            self.decorate_key('sudo'): {
                'model': get_user_model()(
                    # Default Superuser
                    username='admin',
                    first_name='Me Atualize',
                    last_name='Admin',
                    email='admin@atualize.me',
                    is_staff=True,
                    is_superuser=True,
                    password='admin'
                )
            }
        }

    def save(self, element, seeds, key):
        element['model'].set_password(element['model'].password)
        return super().save(element, seeds, key)
