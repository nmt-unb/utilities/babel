from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    def __str__(self):
        if self.is_staff and self.username: return self.username
        return self.full_name()

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)

    def clean(self):
        self.email = self.email.lower()
        self.username = self.username.lower()

    def full_name(self):
        if self.first_name:
            if self.last_name: return f'{self.first_name} {self.last_name}'
            return self.first_name
        return ''
