from copy import deepcopy
from datetime import datetime
import itertools
import json
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from utils.models import Base
from utils.models.fields import basics, composites, entities, relations
from utils.strings.parser import urlify

def csv_path(instance, filename):
    return 'data/csv/{0}/{1}_{2}_{3}.{4}'.format(
        urlify(str(instance.author)),
        urlify(instance.name),
        ('V_%03d' % instance.version),
        datetime.now().strftime('%Y%m%d%H%M%S'),
        filename.split('.')[-1]
    )

class Data(Base):
    """
    CSV with data parsed or required to be parsed
    """

    author = entities.author('datas')
    name = composites.name(max_length=100)
    description = basics.text(_('description'), blank=True)
    csv = basics.file(_('csv sheet'), upload_to=csv_path)
    version = basics.number(_('version'), blank=True, default=1)
    previous_instructions = basics.text(_('previous instructions'), blank=True)
    history = relations.many_to_many('self', verbose_name=_('history'))

    class Meta:
        ordering = ['author', '-updated_at']
        verbose_name = _('data sheet')
        verbose_name_plural = _('data sheets')

    def __str__(self):
        if self.name: return self.name + self.version_verbose()
        return self.version_verbose()

    def version_verbose(self):
        if self.version != 1: return ' ({0} {1})'.format(_('Version'), ('%03d' % self.version))
        return ''

    def parse_and_save(self, instructions):
        new_data = deepcopy(self)
        new_data.pk = None
        try:
            data = self.csv.readlines()
            new_data.version += 1
            new_data.csv = csv_path(new_data, 'file.csv')
            with open((settings.MEDIA_ROOT + new_data.csv.name), 'w') as file:
                new_data.previous_instructions = instructions
                instructions = json.loads(instructions)
                ignore_line = instructions['general']['ignore_first_line']
                for row in data:
                    if not ignore_line:
                        row = ' '.join(row.decode('UTF-8').split())
                        row_data = self.parse_row(row, instructions) + '\n'
                        file.write(row_data)
                    else:
                        ignore_line = False
            self.save()
            new_data.save()
            self.history.add(new_data)
        except:
            print('Failed to parse.')
            raise

    def parse_row(self, row, instructions):
        try:
            data = {}
            structure = instructions['structure']
            map_data_instructions = structure['from']
            self.map_data(data, map_data_instructions['root'], 'root', row, map_data_instructions)
            return self.reduce_data(data, structure['to'])
        except:
            print('Failed to parse row.')
            raise

    def map_data(self, data, element, key, values, instructions):
        try:
            type = element['type']
            if type == 'delimitation': self.perform_delimitation(data, element, key, values, instructions)
            elif type == 'string': self.parse_string(data, key, values)
            elif type == 'integer': self.parse_integer(data, element, key, values)
        except:
            print('Failed to map data.')
            raise

    def perform_delimitation(self, data, element, key, values, instructions):
        try:
            if values:
                data[key] = values
                delimiter = element['delimiter']
                values = values.split(delimiter)
                sub_elements_keys = element['elements']
                for sub_key, value in itertools.zip_longest(sub_elements_keys, values, fillvalue=''):
                    if sub_key != '':
                        self.map_data(data, instructions[sub_key], sub_key, value, instructions)
        except:
            print('Failed to perform delimitation.')
            raise

    def parse_string(self, data, key, value):
        if value:
            data[key] = value
        elif not hasattr(data, key):
            data[key] = ''

    def parse_integer(self, data, element, key, value):
        try:
            if value and not hasattr(data, key):
                data[key] = element['format'].format(int(value))
        except:
            self.parse_string(data, key, value)

    def reduce_data(self, data, to):
        try:
            for key in data:
                to = to.replace('{' + key + '}', data[key])
            return to
        except:
            print('Failed to reduce data.')
            raise
