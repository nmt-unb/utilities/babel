from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from utils.models import Base
from utils.models.fields import basics

class Settings(Base):
    """
    System settings
    """

    seed_hash = basics.text(_('seed hash'), blank=True)

    class Meta:
        verbose_name = _('settings')
        verbose_name_plural = verbose_name

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)

    def clean(self):
        if not self.id and Settings.objects.all():
            error = {'non_field_errors': _('Settings is a singleton.')}
            raise ValidationError(error, code='singleton')

    def add_seed(self, context):
        self.seed_hash += f'[{context}]'
        self.save()

    def has_seed(self, context):
        return self.seed_hash.find(f'[{context}]') != -1
