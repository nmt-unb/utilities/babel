import os

# CORS

# https://docs.djangoproject.com/en/2.0/ref/settings/#allowed-hosts

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS').split(',')

# https://github.com/ottoyiu/django-cors-headers#cors_origin_whitelist

CORS_ORIGIN_WHITELIST = tuple(os.environ.get('CORS_ORIGIN_WHITELIST').split(','))
