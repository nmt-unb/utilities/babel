import os

# Secrets

# https://docs.djangoproject.com/en/2.0/ref/settings/#secret-key

SECRET_KEY = os.environ.get('SECRET_KEY')
