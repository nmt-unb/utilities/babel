# CORS

# https://docs.djangoproject.com/en/2.0/ref/settings/#allowed-hosts

ALLOWED_HOSTS = '*'

# https://github.com/ottoyiu/django-cors-headers#cors_origin_allow_all

CORS_ORIGIN_ALLOW_ALL = True
