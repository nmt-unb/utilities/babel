"""
Django settings for API.

For more information, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

from .base import *

if os.environ.get('DJANGO_ENV') == 'production':
    from .production import *
else:
    from .develop import *
