# Middlewares

# https://docs.djangoproject.com/en/2.0/topics/http/middleware/

MIDDLEWARE = [
    # Django Middlewares
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # Third Party Middlewares
    'corsheaders.middleware.CorsMiddleware'
]
