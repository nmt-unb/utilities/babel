from django.utils.translation import ugettext_lazy as _

# https://jet.readthedocs.io/en/latest/config_file.html

JET_INDEX_DASHBOARD = 'babel.admin.IndexDashboard'

JET_SIDE_MENU_COMPACT = True

JET_SIDE_MENU_ITEMS = [
    {
        'label': _('users'),
        'items': [
            {'name': 'babel.user'}
        ]
    },
    {
        'label': _('babel'),
        'items': [
            {'name': 'babel.data'}
        ]
    }
]

JET_DEFAULT_THEME = 'default'

JET_THEMES = [
    {
        'theme': 'default',
        'color': '#47bac1',
        'title': _('Blue')
    },
    {
        'theme': 'green',
        'color': '#44b78b',
        'title': _('Green')
    }
]
