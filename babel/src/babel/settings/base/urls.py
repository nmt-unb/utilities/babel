# URLs & Endpoints

# https://docs.djangoproject.com/en/2.0/ref/settings/#root-urlconf

ROOT_URLCONF = 'babel.urls'

# https://docs.djangoproject.com/en/2.0/ref/settings/#append-slash

APPEND_SLASH = False

# https://docs.djangoproject.com/en/2.0/ref/settings/#media-url

MEDIA_URL = '/media/'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'
