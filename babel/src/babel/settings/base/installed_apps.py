# Installed Apps

# https://docs.djangoproject.com/en/2.0/ref/settings/#installed-apps

INSTALLED_APPS = [
    # Django Apps
    'jet.dashboard',
    'jet',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sessions',
    'django.contrib.sites',
    # Third Party Apps
    'corsheaders',
    'django_extensions',
    # Project Apps
    'babel',
    'utils'
]
