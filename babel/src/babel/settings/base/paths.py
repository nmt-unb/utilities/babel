import os

# Dirs & Paths

BASE_SETTINGS_DIR = os.path.dirname(os.path.abspath(__file__))

SETTINGS_DIR = os.path.dirname(BASE_SETTINGS_DIR)

BABEL_DIR = os.path.dirname(SETTINGS_DIR)

BASE_DIR = os.path.dirname(BABEL_DIR)

PROJECT_DIR = os.path.dirname(BASE_DIR)

# https://docs.djangoproject.com/en/2.0/ref/settings/#media-root

MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media/')

# https://docs.djangoproject.com/en/2.0/ref/settings/#static-root

STATIC_ROOT = os.path.join(PROJECT_DIR, 'static/')

# https://docs.djangoproject.com/en/2.1/ref/settings/#locale-paths

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale/')
]
