from django.utils.translation import ugettext_lazy as _
from jet.dashboard import modules
from jet.dashboard.dashboard import Dashboard

class IndexDashboard(Dashboard):
    columns = 1

    children = [
        modules.ModelList(
            _('babel'),
            models=[
                'babel.Data'
            ]
        ),
        modules.ModelList(
            _('users'),
            models=[
                'babel.User'
            ]
        ),
        modules.RecentActions(
            _('recent actions'),
            limit=15
        )
    ]
