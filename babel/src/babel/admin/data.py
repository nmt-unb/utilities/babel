from django import forms
from django.contrib.admin import register
from django.utils.translation import ugettext_lazy as _
from utils.admin import WithAuthorAdmin
from utils.admin.mixins import decorate_with_link
from babel.models import Data

class DataForm(forms.ModelForm):
    instructions = forms.CharField(
        label=_('Instructions'),
        help_text=_('Instructions (formatted as JSON) to parse the column of the data sheet.'),
        widget=forms.Textarea,
        required=False
    )

    class Meta:
        model = Data
        fields = ('instructions',)

@register(Data)
class DataAdmin(WithAuthorAdmin):
    form = DataForm
    list_display = ('name', 'version', 'author_link', 'updated_at')
    search_fields = ('name', 'version', 'author__first_name', 'author__last_name', 'author__username')
    fieldsets = (
        (
            None,
            {
                'fields': ('name', 'version', 'description', 'csv')
            }
        ),
        (
            _('Parser options'),
            {
                'fields': ('version_history', 'instructions', 'previous_instructions')
            }
        ),
    )
    readonly_fields = ('version', 'version_history', 'previous_instructions')

    def version_history(self, obj):
        return decorate_with_link(list(obj.history.all()))
    version_history.short_description = _('History')

    def save_model(self, request, obj, form, change):
        instructions = form.cleaned_data.get('instructions', None)
        if instructions:
            obj.parse_and_save(instructions)
        else:
            super().save_model(request, obj, form, change)
