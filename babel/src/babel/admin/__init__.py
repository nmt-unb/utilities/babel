from django.conf import settings
from django.contrib.admin import AdminSite

AdminSite.index_title = 'Babel'
AdminSite.site_header = 'Babel'
AdminSite.site_title = 'Babel'

from .data import DataAdmin
from .index_dashboard import IndexDashboard
from .user import UserAdmin
