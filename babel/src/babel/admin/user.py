from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

@admin.register(get_user_model())
class UserAdmin(DjangoUserAdmin):
    pass
