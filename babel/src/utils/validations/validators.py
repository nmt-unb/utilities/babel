from django.core import validators
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from utils.strings import regex
from . import validate

CNPJ = _('must be a valid CNPJ.')
def cnpj(value):
    if not validate.cnpj(value):
        raise ValidationError(CNPJ.capitalize(), code='cnpj')

CPF = _('must be a valid CPF.')
def cpf(value):
    if not validate.cpf(value):
        raise ValidationError(CPF.capitalize(), code='cpf')

NAME = _('must have only alphabetic characters and space.')
def name(value):
    return validators.RegexValidator(
        regex=r'^[A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]*$',
        message=NAME.capitalize(),
        code='name'
    )(value)

ONLY_NUMBERS = _('must have only numbers.')
def only_numbers(value):
    if not validate.only_numbers(value):
        raise ValidationError(
            ONLY_NUMBERS.capitalize(),
            code='only_numbers'
        )
PHONE = _(r'must match the pattern "(xx) xxxxx-xxxx" with only numbers.')
def phone(value):
    return validators.RegexValidator(
        regex=regex.PHONE,
        message=PHONE.capitalize(),
        code='phone'
    )(value)
