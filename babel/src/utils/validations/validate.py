from utils.strings import parser, regex

def cnpj(value):
    if not regex.valid(regex.CNPJ, value): return False
    value = int(parser.only_number(value))
    return _is_valid_digit(value, 12) and _is_valid_digit(value, 13)

def cpf(value):
    if not regex.valid(regex.CPF, value): return False
    value = int(parser.only_number(value))
    return _is_valid_digit(value, 9) and _is_valid_digit(value, 10)

def only_numbers(value):
    return regex.valid('^[0-9]*$', value)

# CPF and CNPJ
def _is_valid_digit(value, at):
    numbers = value[:at]

    if at > 10:
        # CNPJ case
        degrees = list(range(at-7, 1, -1)) + list(range(9, 1, -1))
    else:
        # CPF case
        degrees = range(at+1, 1, -1)

    sum = 0
    for digit, degree in zip(numbers, degrees):
        sum += int(digit)*degree

    remainder = sum % 11
    if remainder < 2:
        return int(value[at]) == 0
    else:
        return int(value[at]) == (11 - remainder)
