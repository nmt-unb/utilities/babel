from datetime import date
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

def boolean(
    verbose_name='',
    blank=False,
    default=False,
    help_text=''
    ):
    return models.BooleanField(
        verbose_name,
        blank=blank,
        default=default,
        help_text=help_text
    )

def char(
    verbose_name='',
    blank=False,
    choices=None,
    default='',
    editable=True,
    help_text='',
    max_length=100,
    null=False,
    unique=False,
    validators=[]
    ):
    return models.CharField(
        verbose_name,
        blank=blank,
        choices=choices,
        default=default,
        editable=editable,
        help_text=help_text,
        max_length=max_length,
        null=null,
        unique=unique,
        validators=validators
    )

def date(
    verbose_name='',
    auto_now=False,
    auto_now_add=False,
    blank=False,
    default=date.today,
    editable=True,
    help_text=''
    ):
    if auto_now:
        return models.DateField(
            verbose_name,
            auto_now=auto_now,
            blank=blank,
            editable=editable,
            help_text=help_text
        )
    elif auto_now_add:
        return models.DateField(
            verbose_name,
            auto_now_add=auto_now_add,
            blank=blank,
            editable=editable,
            help_text=help_text
        )
    else:
        return models.DateField(
            verbose_name,
            blank=blank,
            default=default,
            editable=editable,
            help_text=help_text
        )

def date_time(
    verbose_name='',
    auto_now=False,
    auto_now_add=False,
    blank=False,
    default=timezone.now,
    editable=True,
    help_text=''
    ):
    if auto_now:
        return models.DateTimeField(
            verbose_name,
            auto_now=auto_now,
            blank=blank,
            editable=editable,
            help_text=help_text
        )
    elif auto_now_add:
        return models.DateTimeField(
            verbose_name,
            auto_now_add=auto_now_add,
            blank=blank,
            editable=editable,
            help_text=help_text
        )
    else:
        return models.DateTimeField(
            verbose_name,
            blank=blank,
            default=default,
            editable=editable,
            help_text=help_text
        )

def file(verbose_name='', upload_to=''):
    return models.FileField(
        upload_to=upload_to,
        verbose_name=verbose_name
    )

def float(
    verbose_name,
    blank=False,
    default=0.0,
    help_text='',
    null=False
    ):
    return models.FloatField(
        verbose_name,
        blank=blank,
        default=default,
        help_text=help_text,
        null=null
    )

def image(verbose_name='', upload_to=''):
    return models.ImageField(
        upload_to=upload_to,
        verbose_name=verbose_name
    )

def number(
    verbose_name,
    blank=False,
    default=0,
    help_text='',
    max_value=None,
    min_value=None,
    null=False,
    validators=[],
    unique=False
    ):
    local_validators = []
    if max_value: local_validators += [MaxValueValidator(max_value)]
    if min_value: local_validators += [MinValueValidator(min_value)]
    return models.PositiveIntegerField(
        verbose_name,
        blank=blank,
        default=default,
        help_text=help_text,
        null=null,
        validators=local_validators,
        unique=unique
    )

def text(verbose_name, blank=False, default='', help_text=''):
    return models.TextField(
        verbose_name,
        blank=blank,
        default=default,
        help_text=help_text
    )

def url(verbose_name=_('url'), blank=False, default='', help_text=''):
    return models.URLField(
        verbose_name,
        blank=blank,
        default=default,
        help_text=help_text
    )
