from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from . import relations

def author(
    related_name='',
    verbose_name=_('author'),
    on_delete=models.SET_NULL,
    validators=[]
    ):
    return relations.foreign_key(
        settings.AUTH_USER_MODEL,
        blank=True,
        editable=False,
        null=True,
        on_delete=on_delete,
        related_name=related_name,
        validators=validators,
        verbose_name=verbose_name
    )
