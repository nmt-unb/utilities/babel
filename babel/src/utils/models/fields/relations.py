from django.db import models

def foreign_key(
    entity='',
    blank=False,
    editable=True,
    null=True,
    on_delete=models.SET_NULL,
    related_name=None,
    validators=[],
    verbose_name=''
    ):
    return models.ForeignKey(
        entity,
        blank=blank,
        editable=editable,
        null=null,
        on_delete=on_delete,
        related_name=related_name,
        validators=validators,
        verbose_name=verbose_name
    )

def many_to_many(
    entity='',
    related_name=None,
    verbose_name=''
    ):
    return models.ManyToManyField(
        entity,
        related_name=related_name,
        verbose_name=verbose_name
    )

def one_to_one(
    entity='',
    null=True,
    on_delete=models.SET_NULL,
    primary_key=False,
    related_name=None,
    verbose_name=''
    ):
    if primary_key:
        null = False
        on_delete = models.CASCADE
    return models.OneToOneField(
        entity,
        null=null,
        on_delete=on_delete,
        primary_key=primary_key,
        related_name=related_name,
        verbose_name=verbose_name
    )
