from django.utils.translation import ugettext_lazy as _
from utils.validations import validators
from . import basics

def cnpj():
    return basics.char(
        _('company id'),
        max_length=14,
        default='',
        help_text=_('Brazilian company id (CNPJ).'),
        unique=True,
        validators=[validators.cnpj]
    )

def cpf():
    return basics.char(
        _('social security id'),
        max_length=11,
        default='',
        help_text=_('Brazilian social security id (CPF).'),
        unique=True,
        validators=[validators.cpf]
    )

def created_at():
    return basics.date_time(
        _('date of creation'),
        auto_now_add=True,
        editable=False
    )

def is_active(
    verbose_name=_('active'),
    blank=False,
    default=True,
    help_text=''
    ):
    return basics.boolean(
        verbose_name,
        blank=blank,
        default=default,
        help_text=help_text
    )

def name(
    verbose_name=_('name'),
    blank=False,
    default='',
    help_text='',
    max_length=50,
    unique=False
    ):
    return basics.char(
        verbose_name,
        blank=blank,
        default=default,
        help_text=help_text,
        max_length=max_length,
        unique=unique,
        validators=[validators.name]
    )

def number_char(
    verbose_name='',
    blank=False,
    default='',
    help_text='',
    max_length=10,
    validators=[]
    ):
    return basics.char(
        verbose_name,
        blank=blank,
        default=default,
        help_text=help_text,
        max_length=max_length,
        validators=[validators.only_numbers] + validators
    )

def phone(
    verbose_name=_('phone number'),
    default='',
    help_text=''
    ):
    return basics.char(
        verbose_name,
        default=default,
        help_text=help_text,
        max_length=15,
        validators=[validators.phone]
    )

def updated_at():
    return basics.date_time(
        _('date from last change'),
        auto_now=True,
        editable=False
    )
