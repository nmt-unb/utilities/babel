from django.db import models
from .fields import composites

class Timestamp(models.Model):
    created_at = composites.created_at()
    updated_at = composites.updated_at()

    class Meta:
        abstract = True
