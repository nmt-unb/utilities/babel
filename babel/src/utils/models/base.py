from .timestamp import Timestamp

class Base(Timestamp):
    class Meta:
        abstract = True
