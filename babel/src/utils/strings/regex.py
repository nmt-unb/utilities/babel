import re

CNPJ = r'^[0-9]{2}.[0-9]{3}.[0-9]{3}/[0-9]{4}-[0-9]{2}$'
CPF = r'^[0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}$'
PHONE = r'^\([0-9]{2}\) [0-9]{5}\-[0-9]{4}$'

def clear(regex, string):
    return re.sub(regex, '', string) if regex and string else ''

def change(regex, to, string):
    return re.sub(regex, to, string) if regex and to and string else ''

def valid(regex, string):
    match = re.fullmatch(regex, string)
    return match != None
