from . import regex

def capitalize(string):
    return string.capitalize() if string else ''

def join_each_true(elements=[], delimiter=' '):
    return delimiter.join(str(element) for element in elements if element)

def only_number(string):
    return regex.clear(r'[^0-9]', string)

def urlify(string):
    string = regex.clear(r'[^\w\s]', string)
    return regex.change(r'\s+', '-', string)
