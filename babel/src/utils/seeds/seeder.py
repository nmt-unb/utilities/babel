class Seeder():
    name = ''
    name_plural = ''
    verbose_name = ''
    verbose_name_plural = ''
    context = 'base'
    enable_post_save = False
    enable_post_seed_save = False
    verbosity_level = 2

    def elements(self):
        return {}

    def seed(self, seeds):
        self.verify_names()
        self.inform_title()
        if self.name_plural not in seeds: seeds[self.name_plural] = {}
        elements = self.elements()
        for key in elements: self.perform_seed(elements[key], seeds, key)
        if self.enable_post_seed_save:
            for key in elements: self.post_seed_save(elements[key], seeds, key)

    def perform_seed(self, element, seeds, key):
        seeds[self.name_plural][key] = self.save(element, seeds, key)
        if self.enable_post_save: self.post_save(element, seeds, key)
        self.inform_persistance(key)

    def save(self, element, seeds, key):
        model = element['model']
        model.save()
        return model

    def post_save(self, element, seeds, key):
        pass

    def post_seed_save(self, element, seeds, key):
        pass

    def decorate_key(self, key, context=None):
        if not context: context = self.context
        if context: return f'{context}_{key}'
        else: return key

    def verify_names(self):
        if not self.name_plural: self.name_plural = self.name + 's'
        if not self.verbose_name: self.verbose_name = self.name
        if not self.verbose_name_plural: self.verbose_name_plural = self.verbose_name + 's'

    def inform_title(self):
        if self.verbosity_level > 0: print(f'\n## Seeding {self.verbose_name_plural.capitalize()}\n')

    def inform_persistance(self, key):
        if self.verbosity_level > 1: print(f'- {self.verbose_name.capitalize()} "{key}" persisted')
