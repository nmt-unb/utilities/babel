# https://stackoverflow.com/a/46527083
from django.contrib.admin.templatetags.admin_urls import admin_urlname
from django.shortcuts import resolve_url
from django.utils.html import format_html
from django.utils.translation import ugettext as _

def decorate_related_with_link(obj, field_name):
    related_obj = getattr(obj, field_name)
    if callable(related_obj):
        return decorate_with_link(related_obj())
    else:
        return decorate_with_link(related_obj)

def decorate_with_link(obj):
    if obj:
        if isinstance(obj, list):
            return format_html(', '.join([decorate_with_link(item) for item in obj]))
        else:
            id = obj.id
            if callable(id): id = id()
            url = resolve_url(admin_urlname(obj._meta, 'change'), id)
            return format_html('<a href="{}" class="changelink">{}</a>', url, str(obj))
    else:
        return None

class RelatedObjectLinkMixin(object):
    """
    Generate links to related links. Add this mixin to a Django admin model. Add a 'link_fields' attribute to the admin
    containing a list of related model fields and then add the attribute name with a '_link' suffix to the
    list_display attribute. For Example a Student model with a 'teacher' attribute would have an Admin class like this:

    class StudentAdmin(RelatedObjectLinkMixin, ...):
        link_fields = ['teacher']

        list_display = [
            ...
            'teacher_link'
            ...
        ]
    """

    link_fields = []

    def get_link_fields(self):
        return self.link_fields

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.get_link_fields():
            for field_name in self.get_link_fields():
                func_name = field_name + '_link'
                setattr(self, func_name, self._generate_link_func(field_name))

    def _generate_link_func(self, field_name):
        def _link(obj, *args, **kwargs):
            return decorate_related_with_link(obj, field_name)
        _link.__name__ = _(field_name)
        return _link
