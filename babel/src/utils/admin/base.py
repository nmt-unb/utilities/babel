from django.contrib import admin
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from .mixins import RelatedObjectLinkMixin

class BaseAdmin(RelatedObjectLinkMixin, admin.ModelAdmin):
    date_hierarchy = 'created_at'
    entity_fields = ('id', 'created_at', 'updated_at')
    extra_entity_fields = None

    def get_link_fields(self):
        return self.link_fields

    def get_readonly_fields(self, request, obj):
        if self.readonly_fields: return ('id', 'created_at', 'updated_at') + self.readonly_fields
        return ('id', 'created_at', 'updated_at')

    def get_entity_fieldset(self):
        if self.extra_entity_fields: fields = self.extra_entity_fields + self.entity_fields
        else: fields = self.entity_fields
        return ((_('Entity info'), {'fields': fields}),)

    def get_fieldsets(self, request, obj):
        if self.fieldsets: return self.fieldsets + self.get_entity_fieldset()
        elif self.fields: return ((None, {'fields': self.fields}),) + self.get_entity_fieldset()
        else: return super().get_fieldsets(request, obj)

    def get_list_display(self, list_display):
        return ('edit',) + self.list_display

    def get_list_display_links(self, request, list_display):
        return ('edit',) + self.list_display_links

    def edit(self, obj):
        return format_html(f'<center><span class="icon-edit"/></center>')
    edit.short_description = ''
