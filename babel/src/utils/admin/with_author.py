from django.contrib import admin
from .base import BaseAdmin
from .mixins import RelatedObjectLinkMixin

class WithAuthorAdmin(BaseAdmin):
    extra_entity_fields = ('author_link',)

    def get_link_fields(self):
        if super().get_link_fields(): return ('author',) + super().get_link_fields()
        return ('author',)

    def get_readonly_fields(self, request, obj):
        return ('author_link',) + super().get_readonly_fields(request, obj)

    def save_model(self, request, obj, form, change):
        if not obj.author: obj.author = request.user
        super().save_model(request, obj, form, change)
