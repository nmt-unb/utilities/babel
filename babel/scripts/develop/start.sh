#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

printf "\n## Compiling Translations\n\n"

python manage.py compilemessages

printf "\n## Collecting Statics\n\n"

python manage.py collectstatic --clear --no-input

printf "\n## Performing Migrations\n\n"

python manage.py migrate

printf "\n## Starting Server\n\n"

python manage.py runserver 0.0.0.0:8000
