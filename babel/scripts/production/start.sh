#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

printf "\n## Performing Migrations\n\n"

python manage.py migrate

printf "\n## Starting Gunicorn\n\n"

gunicorn mercado.wsgi -w 8 -b 0.0.0.0:80
